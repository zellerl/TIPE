
#cette version est loin d'être terminée, en particulier le champ de background n'apparaît pas encore  (pas important pour l'IPT)

#ici, c'est des "humains" qui sont modélisés sortant d'une piece (sortie à gauche avec un obstacle)
#les regles d'avancer seront donc à changer

import matplotlib.pyplot as plt
import matplotlib.animation as animation
from copy import deepcopy
import random

dimx=20
dimy=20

fig = plt.figure()
plt.axis([0,dimx, 0, dimy])

def init() :
    line=plt.plot([25,26],[25,25,],'sk', markersize = 4)
    return line

duree=1000
t=0
M=[[0 for i in range (dimy)] for j in range (dimx)]  #présence des pietons
E=[[0 for i in range (dimy)] for j in range (dimx)]#etat des pietons dans les case

S=[(1,0),(0,-1),(0,0),(0,1),(-1,0)]

for i in range (dimx):
    E[i][dimy-3]=[1, 0, 0]   #informations sur un pieton : [humeur (1=happy), temps depuis le dernier changement, nombre de refus essuyés]
    M[i][dimy-3]=1
    E[i][dimy-18]=[1, 0, 0]   #informations sur un pieton : [humeur, temps depuis le dernier changement, nombre de refus essuyés]
    M[i][dimy-18]=1
    E[i][dimy-7]=[1, 0, 0]   #informations sur un pieton : [humeur, temps depuis le dernier changement, nombre de refus essuyés]
    M[i][dimy-7]=1
    E[i][dimy-9]=[1, 0, 0]   #informations sur un pieton : [humeur, temps depuis le dernier changement, nombre de refus essuyés]
    M[i][dimy-9]=1
portey=[int(dimx/2),int(dimx/2)+1]
portex=[0,0]



    

Msuite=[M]

def calcul_Tij_base (i,j):
    dy=j-portey[0]   #à modifier si la porte est élargie
    dx=i-portex[0]
    if abs(dx)>abs(dy) :
        Tij=[0,0,0,0,1]   
    elif dy<0 :
        Tij=[0,0,0,1,0]
    else : Tij=[0,1,0,0,0]
    return(Tij)

def maj_humeur(i,j) :
        if E[i][j][2]>=4 :
            if E[i][j][0]==1 :
                E[i][j][0]=0
                E[i][j][1]=0
                
            else :
                E[i][j][0]=1
                E[i][j][1]=0
                E[i][j][2]=0

while t<duree :
    t+=1
    for i in range (dimx):
        for j in range (dimy):
            bordxg=(i==0)
            bordxd=(i==dimx-1)
            bordyb=(j==0)
            bordyh=(j==dimy-1)
            if i in portex and j in portey :
                    M[i][j]=0
            elif M[i][j]!= 0 and not bordxg and not bordxd and not bordyh and not bordyb :
                T=calcul_Tij_base (i,j)
                if E[i][j][0]==0 :
                    for k in range (len(T)):
                        T[k]+=6*random.random()
                        print(T)
                dep=random.choices([(1,0),(0,-1),(0,0),(0,1),(-1,0)], T)
                #dep=random.choice([(1,0),(0,-1),(0,0),(0,1),(-1,0)])   #pour mon ordinateur personnel
                ni,nj=i+dep[0][0],j+dep[0][1] #nouvelle position choisie
                if M[ni][nj]!=0 :
                    E[i][j][2]+=1
                    maj_humeur(i,j)
                    
                else :
                    E[ni][nj]=E[i][j]
                    E[i][j]=0
                    E[ni][nj][1]+=1
                    M[ni][nj]=1
                    M[i][j]=0
                    maj_humeur(ni,nj)
                
                
                

                  
    Msuite.append(deepcopy(M))

def animate (i):
    x=[];y=[]
    M=Msuite[i]
    for k in range (dimx):
        for j in range (dimy):
            if M[k][j]==1 :
                x.append(k)
                y.append(j)
    line=plt.plot(x,y,'sk',markersize=8)
    return line

ani=animation.FuncAnimation(fig, animate, init_func=init, frames=duree, interval=500, blit=True, repeat=True)

plt.show()
                















            


                            
