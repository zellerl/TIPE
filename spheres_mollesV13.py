
#à finir de débuguer avant d'ajouter les calculs des forces

#les billes sont désignées par leur position dans tabBilles

class Bille :
    def __init__(self, num, casx, casy, posx, posy, vitx, vity, accx, accy) :
        self.num=num
        self.casx=[int(casx)]+[0]*nbIterations  #le int() permet d'eviter une erreur "list indices must be integers or slices, not float" dans contacts (pourquoi ? jsp)
        self.casy=[int(casy)]+[0]*nbIterations
        self.posx=[posx]+[0]*nbIterations
        self.posy=[posy]+[0]*nbIterations
        self.vitx=[vitx]+[0]*nbIterations
        self.vity=[vity]+[0]*nbIterations
        self.accx=[accx]+[0]*nbIterations
        self.accy=[accy]+[0]*nbIterations

import numpy as np
import matplotlib.pyplot as plt                                                                                                                                                         ##à faire : forces de contacts
from random import *                      ##modifs à faire : contact dans la même cases
from math import *
from matplotlib import animation
from copy import deepcopy


                              
n=2
tailleBille=0.7
tailleCase = 0.7
nbCasesl=2
nbCasesp=2
larg=nbCasesl*tailleCase ##largeur piece
prof=nbCasesp*tailleCase ##profondeur piece
largeurPorte=1
porte=[(larg/2)-largeurPorte,(larg/2)+largeurPorte] ##coordonnées de la porte 
nbIterations=2000
dt=0.5

att_por=6

tabBilles=np.zeros(n, dtype=Bille)
for i in range (n) : #pour éviter d'avoir une unique bille n fois
    tabBilles[i]=Bille(i,0,0,0,0,0,0,0,0)
matriceCasesInit=[[[] for i in range (nbCasesl)]for j in range (nbCasesp)]
tabCases=[1,deepcopy(matriceCasesInit), deepcopy(matriceCasesInit)] ##numero des billes dans les cases où elles sont
          # à chaque itération on remplit la matrice désignée

billesSorties =[]
            
Ap = 480
Bp = 15
A = 2000
B = 0.5
r = 0.35
ro = 0.1
m = 80
K = 1
k = 2.4e4
#kpieton = 2e-2
v0 = 3.0
tau = 0.5
#alpha = 20


seed(0)

def initialisation():
    print("init casesinit", matriceCasesInit)
    for i in range(n):
        xr=random()
        yr=random()
        tabBilles[i].posx[0]=xr*larg
        tabBilles[i].posy[0]=yr*(prof*0.5)+(prof/2)
        casx=int(xr*nbCasesl)
        casy=int(yr*nbCasesl)
        tabBilles[i].casx[0]=casx
        tabBilles[i].casy[0]=casy
        tabCases[1][casx][casy].append(i)
    print("tabCases", tabCases)
        
        
        
def contacts(bil, t) :
    i,j=bil.casy[t], bil.casx[t]
    print("fonction contacts appellee pour (num, i, j)",bil.num, i, j)
    listeBillesContacts =[]

    if i<(nbCasesp-1) and i>0:
        if j<(nbCasesl-1) and j>0 :
            atester = [[i-1,j-1],[i-1,j],[i-1,j+1],[i,j-1],[i,j+1],[i+1,j-1],[i+1,j],[i+1,j+1]] ##liste des cases adjacentes
        elif j<(nbCasesl-1) :
            atester= [[i-1,j],[i-1,j+1],[i,j+1],[i+1,j],[i+1,j+1]]
        else : atester = [[i-1,j-1],[i-1,j],[i,j-1],[i+1,j-1],[i+1,j]]
    if i==0 :
        if j<(nbCasesl-1) and j>0 : 
            atester = [[i,j-1],[i,j+1],[i+1,j-1],[i+1,j],[i+1,j+1]]
        elif j<(nbCasesl-1) :
            atester= [[i,j+1],[i+1,j],[i+1,j+1]]
        else : atester = [[i,j-1],[i+1,j-1],[i+1,j]]
    else :
        if j<(nbCasesl-1) and j>0 :
            atester =[[i-1,j-1],[i-1,j],[i-1,j+1],[i,j-1],[i,j+1]]
        elif j==(nbCasesl-1) :
            atester= [[i-1,j-1],[i-1,j],[i,j-1]]
        else : atester =[[i-1,j],[i-1,j+1],[i,j+1]]
    atester.append([i,j]) 
    e=(tabCases[0]%2)+1
    print("atester, e, tabCases :", atester, e, tabCases)
    for k in atester :
        #print(" tabCases",tabCases[e])
        c=tabCases[e][k[0]][k[1]]
        print(k,listeBillesContacts, c)
        for l in c :
            if l!=bil.num :
                listeBillesContacts.append(l)
                print("listeContacts :" ,listeBillesContacts) 
    return listeBillesContacts


def vecteur (xa, ya, xb, yb, l) :
    ##calcul un vecteur de longueur l sur la direction (AB)
    vecteur = [xb-xa, yb-ya]
    theta= atan(vecteur[0]/vecteur[1])
    x=l*cos(theta)
    y=l*sin(theta)
    return x,y
                  

def a_forcecontact(xa,ya,xb,yb):
    dist=sqrt((xb-xa)**2+(yb-ya)**2)
    if dist==0 or ya==yb :
        print("tente de relancer, il y a eu une division par zero pour", xa,ya,xb,yb)
    else :
        f = A*exp((2*r-dist)/B) + k*(2*r-dist)
        fx = f/dist*(xb-xa)
        fy = f/dist*(yb-ya)
        return(fx/m,fy/m)
    


def a_att_por(x,y):
    dip=sqrt((porte[0]-x)**2+(porte[1]-y)**2)
    f = (-Ap*exp((r-dip)/Bp))
    fx = f/dip*(x-porte[0])
    fy = f/dip*(y-porte[1])
    return(fx/m, fy/m)


    

def majvpc(t,i): ## maj de la vitesse, position, case de i=tabBille[k] après calcul de forces qui agissent sur elle
    con=contacts(i,t) ## liste des num des billes contactes
    print("contacts :", con)
    (aix,aiy)=a_att_por(i.posx[t],i.posy[t])
    print("l'attraction de la porte est de : ",(aix,aiy))  
    for k in con:
        print("k", k)
        xi,yi=i.posx[t],i.posy[t]
        xk=tabBilles[k].posx[t]
        yk=tabBilles[k].posy[t]
        (aixk,aiyk)=a_forcecontact(xi, yi, xk, yk)            ## force de contact à definir 
        aix+=aixk
        aiy+=aiyk
    i.accx[t+1]=aix
    i.accy[t+1]=aiy
    i.vitx[t+1]=aix*dt+i.vitx[t]
    i.vity[t+1]=aiy*dt+i.vity[t]
    i.posx[t+1]=i.vitx[t]*dt+i.posx[t]
    i.posy[t+1]=i.vity[t]*dt+i.posx[t]
    casx,casy = int(i.posx[t+1]/nbCasesl),int(i.posy[t+1]/nbCasesp)
    i.casx[t+1]=casx
    i.casy[t+1]=casy
    e=tabCases[0]
    print("casx, casy, tabCases, i.num :", casx, casy,  tabCases, i.num)
    if not casx>=0 and casx<=nbCasesl and casy >=0 and casy<=nbCasesp : ##peut etre pas bien fait, A VERIFIER
        billesSorties.append(i.num)
    else : tabCases[e][casx][casy].append(i.num) 

     
def tourne (tmax):
    for t in range (tmax-1):
#        print("tabCases", tabCases)
        tabCases[0]=(tabCases[0] % 2) +1
        tabCases[tabCases[0]]=deepcopy(matriceCasesInit)
#        print("tabCases", tabCases)
        for b in range (n) :
            majvpc(t, tabBilles[b])
            print("UN PIETON DE FINI ! le numero, tabCases, à l'instant :", b,tabCases, t)
        
    
    


def affichagepiece():
    murdufondx = np.linspace(0.,larg,500)
    plt.plot(murdufondx, np.zeros(500)+prof,'k')
    
    murdudevant1=np.linspace(0.,porte[0], 500)
    plt.plot(murdudevant1, np.zeros(500),'k')
    
    murdudevant2=np.linspace(porte[1],larg,500)
    plt.plot(murdudevant2, np.zeros(500),'k')
    
    basemurverticalx=np.linspace(0.,0.01,500)
    basemurverticaly=np.linspace(0.,prof, 500)
    plt.plot(basemurverticalx,basemurverticaly,'k')
    plt.plot(basemurverticalx + larg , basemurverticaly, 'k')
    

def affichage(t):
    affichagepiece()
    for p in range (n):
        plt.plot(tabBilles[p].posx[t],tabBilles[p].posy[t] ,'bo')##n'affiche que la position de départ pour l'instant
    plt.show()

## pour tester :
initialisation()

billeT=Bille(1,1,0.8,0.8,0,0,0,0,0)
##a=contacts(billeT, 0)
##print("a=", a)
tourne(10)

















